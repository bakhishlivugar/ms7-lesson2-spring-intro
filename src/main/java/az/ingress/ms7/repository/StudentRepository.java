package az.ingress.ms7.repository;

import az.ingress.ms7.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository
        extends JpaRepository<Student, Long> {
}
