package az.ingress.ms7.services;

import az.ingress.ms7.dto.CreateStudentDto;
import az.ingress.ms7.dto.StudentDto;

public interface StudentService {

    StudentDto getStudent(Long id);

    StudentDto create(CreateStudentDto studentDto);

    StudentDto update(Long id, CreateStudentDto studentDto);

    void delete(Long id);
}
